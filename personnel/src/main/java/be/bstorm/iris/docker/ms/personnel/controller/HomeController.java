package be.bstorm.iris.docker.ms.personnel.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping(path = {"", "/", "/home"})
    public String helloAction() {
        return "Hello world !";
    }
}
