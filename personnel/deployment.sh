#!/bin/bash

mvnw clean install
echo "Build updated image"
docker build -t docker_iris-personnel .
docker run --name docker_iris-personnel_1 -d --network="docker_iris-backend" docker_iris-personnel
docker stop docker_iris-personnel_2 && docker rm docker_iris-personnel_2
